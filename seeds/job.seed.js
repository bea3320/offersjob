const mongoose = require('mongoose');
const db = require('../db');
const Job = require('../models/Job');

const jobs = [
    {
        contactEmail:"bea@upgrade.com",
        title: "Frontend Developer ( Junior) en Madrid.",
        description: "Adhara Somos una compañía de tecnología centrada en la creación de soluciones en tiempo real para la gestión de liquidez global multidivisa FX y pagos internacionales basados en la tokenización sobre smart contracs. Nos dirigimos principalmente a incluye instituciones financieras y tesorerías corporativas ofreciendo soluciones bancarias compatibles basadas en tecnología blockchain.",
        company:"ADHARA",
        city:"Madrid",
    },
    {
        contactEmail:"diego@upgrade.com",
        title: "Frontend developer",
        description: "Nuestro cliente es una multinacional que tiene un centro especializado en el desarrollo de productos de SW apalancados en los datos y modelos analíticos.El objetivo del equipo de trabajo es desarrollar activos para que permitan aportar valor a sus clientes, generar eficiencias operativas y desarrollar un modelo de negocio basado en la suscripción.",
        company:"Digihunting",
        city:"Sevilla"
    },
    {
        contactEmail:"mario@upgrade.com",
        title: "Junior Frontend Software Engineer",
        description: "Somos un constructor de empresas recién nacido, enfocado en la construcción de nuevos negocios, tratando de revolucionar las industrias relacionadas utilizando los beneficios de las tecnologías de contabilidad descentralizada (DLT) y Blockchain.",
        company:"IO Builders",
        city:"Madrid"
    },
    {
        contactEmail:"ainhoa@upgrade.com",
        title: "Desarrollador/a Frontend Junior",
        description: "Te buscamos a ti, si eres un/a Desarrollador/a Frontend Junior con una experiencia mínima de 1 año y con conocimientos en Angular, para trabajar en una importante Compañía informática en Madrid.",
        company:"Innovando 2Gether",
        city:"Asturias"
    },
];

mongoose
    .connect(db.DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
        const allJobs = await Job.find();
        console.log(allJobs);

        if(allJobs.length) {
            await Job.collection.drop();
        }
    })
    .catch(error => {
        console.log('Error deleting data:', error)
    })
    .then(async () => {
        await Job.insertMany(jobs)
    })
    .catch(error => {
        console.log('Error creating data:', error)
    })
    .finally(() => mongoose.disconnect());