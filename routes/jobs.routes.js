const express = require('express');
const Job = require('../models/Job');
const router = express.Router();


router.get('/', async (req, res, next) => {
    try{
        const jobs = await Job.find();
        return res.render('jobs.hbs', {jobs, user: req.user});
    }catch(error) {
        console.log(error);
        next (new Error(error))
    }
});

router.get('/:id', async (req, res, next) => {
    try{
        const {id} = req.params;
        const job = await Job.findById(id);
        if(job) {
            return res.render('job.hbs', {job, user: req.user});
        }
        return res.status(404).json("No job found for this id");
    }catch(error){
        next(new Error(error));
    }
});

router.get('/city/:city', async(req, res, next) => {
    try{
        const { city } = req.params;
        const jobsByCity = await Job.find( {city});
        return res.json(jobsByCity);
    }catch(error) {
        next(new Error(error))
    }
});

router.post('/create', async (req, res, next) => {
    try{
        const { contactEmail, title, description, company, city, creatorId =  req.user._id} = req.body;
        const newJob = await new Job({contactEmail, title, description, company, city, creatorId});
        const createdJob = await newJob.save();
        return res.render('index.hbs');
    }catch(error) {
        next(new Error(error))
    }
});

router.post('/:id', async (req, res, next) => {
    try{
        const {id} = req.params;
        const deleted = await Job.findByIdAndDelete(id);

        if (deleted) {
            return res.status(200).render('delete.hbs');
        }
        return res.status(200).json('Job not found');
    }catch(error){
        next(new Error(error))
    }
});

router.delete('/:id', async (req, res, next) => {
    try{
        const {id} = req.params;
        const deleted = await Job.findByIdAndDelete(id);

        if (deleted) {
            return res.status(200).json('Job Deleted')
        }
        return res.status(200).json('Job not found');
    }catch(error){
        next(new Error(error))
    }
});

module.exports = router;